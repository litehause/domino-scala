organization := "ru.litehause.domino"
name         := "domino"
version      := "1.0-SNAPSHOT"

scalaVersion := "2.11.8"
scalacOptions ++= Seq("-deprecation", "-feature", "-unchecked")

// Xitrum requires Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

//------------------------------------------------------------------------------
libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.6"
