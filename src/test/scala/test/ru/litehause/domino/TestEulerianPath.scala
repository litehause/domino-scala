package test.ru.litehause.domino

import org.scalatest._
import ru.litehause.domino.model.DominoTile
import ru.litehause.domino.service.EulerianPath

class TestEulerianPath extends FlatSpec with Matchers {

  it should "Test 1 verify proper operation on empty list" in {
    assert(!EulerianPath.hasEulerianPath(Nil))
  }

  it should "Test 2 generate Eulerian path true " in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, None, Some(1)) ::
      DominoTile(3, Some(1), None) :: Nil
    assert(EulerianPath.hasEulerianPath(tiles))
  }

  it should "Test 3 true" in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, None, Some(1)) ::
      DominoTile(3, Some(1), None) ::
      DominoTile(4, Some(1), Some(1)):: Nil
    assert(EulerianPath.hasEulerianPath(tiles))
  }


  it should "Test 4 failed" in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, None, Some(1)) ::
      DominoTile(3, Some(1), None) ::
      DominoTile(4, Some(3), Some(4)) :: Nil
    assert(!EulerianPath.hasEulerianPath(tiles))
  }


  it should "Test 5 failed" in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, None, Some(1)) ::
      DominoTile(3, Some(1), None) ::
      DominoTile(4, Some(4), Some(4)) :: Nil
    assert(!EulerianPath.hasEulerianPath(tiles))
  }


}
