package test.ru.litehause.domino

import org.scalatest._
import ru.litehause.domino.model.DominoTile
import ru.litehause.domino.utils.SetGenerator

class TestGenerator extends FlatSpec with Matchers{

  it should "Test 1 failed generate one element" in {
    intercept[Exception] {
      SetGenerator.generate(1)
    }
  }

  it  should "Test 2 generate 3 elements (None None), (Some(1), None), (Some(1) Some(1))" in {
    val tiles = SetGenerator.generate(3)
    assert(tiles == List(DominoTile(0, None, None), DominoTile(1, Some(1), None), DominoTile(2, Some(1), Some(1))))
  }

  it should "Test 3 generate 28 element equals (Some(6) Some(6))" in {
    val tiles = SetGenerator.generate(28)
    assert(tiles.last == DominoTile(27, Some(6), Some(6)))
  }

  it should "Test 4 8 element equals (Some(3), Some(1)) " in {
    assert(SetGenerator.generate(8).last == DominoTile(7, Some(3), Some(1)))
  }

  it should "Test 5 18 element equal (Some(5), Some(2)) " in {
    assert(SetGenerator.generate(18).last == DominoTile(17, Some(5), Some(2)))
  }
}
