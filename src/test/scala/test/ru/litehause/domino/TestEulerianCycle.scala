package test.ru.litehause.domino

import org.scalatest._
import ru.litehause.domino.model.DominoTile
import ru.litehause.domino.service.{EulerianCycle, EulerianPath}

class TestEulerianCycle extends FlatSpec with Matchers {

  it should "Test 1 failed empty array" in {
    assert(!EulerianCycle.hasEulerianCycle(Nil))
  }


  it should "Test 2 successful " in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, Some(1), Some(2)) ::
      DominoTile(3, None, Some(1)) ::
      DominoTile(4, Some(1), Some(1)) ::
      DominoTile(5, Some(2), None) :: Nil
    assert(EulerianCycle.hasEulerianCycle(tiles))
  }

  it should "Test 3 successful" in {
    val tiles = DominoTile(6, None, None) ::
      DominoTile(7, Some(1), Some(2)) ::
      DominoTile(8, None, Some(1)) ::
      DominoTile(9, Some(1), Some(1)) ::
      DominoTile(10, Some(2), None) ::
      DominoTile(11, Some(3), Some(2)) ::
      DominoTile(12, Some(2), Some(2)) ::
      DominoTile(13, Some(2), Some(3)) ::
      Nil
    assert(EulerianCycle.hasEulerianCycle(tiles))
  }

  it should "Test 4 failed" in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, Some(1), Some(2)) ::
      DominoTile(3, None, Some(1)) ::
      DominoTile(4, Some(1), Some(1)) ::
      DominoTile(5, Some(2), None) ::
      DominoTile(6, Some(3), Some(2)) ::
      DominoTile(7, Some(2), Some(2)) ::
      DominoTile(8, Some(4), Some(3)) ::
      Nil
    assert(!EulerianCycle.hasEulerianCycle(tiles))
  }

  it should "Test 5 failed" in {
    val tiles = DominoTile(1, None, None) ::
      DominoTile(2, Some(1), Some(2)) ::
      DominoTile(3, None, Some(1)) ::
      DominoTile(4, Some(1), Some(1)) ::
      DominoTile(5, Some(2), None) ::
      DominoTile(6, Some(3), Some(2)) ::
      DominoTile(7, Some(4), Some(2)) ::
      DominoTile(8, Some(4), Some(3)) ::
      DominoTile(9, Some(5), Some(5)) ::
      Nil
    assert(!EulerianCycle.hasEulerianCycle(tiles))
  }
}
