package ru.litehause.domino

import java.util.concurrent._

import ru.litehause.domino.service.{EulerianCycle, EulerianPath, GraphGenerator}
import ru.litehause.domino.utils.SetGenerator


object Main {

  def main(args: Array[String]) {

    if (args.length != 1) {
      throw new RuntimeException("insufficient parameters")
    }
    val countDominoTile = args(0).toInt
    if (countDominoTile < 2 && countDominoTile > 28) {
      throw new RuntimeException("domino numbers 2 < N < 28")
    }
    val dominSet = SetGenerator.generate(countDominoTile)

    //Если мы можем собрать костяшки в круг то проверять далее нет смысла так как прямую мы точно сможем построить
    // а раз прямую мы сможем построить то гарантированно сможем их выложить по правилам домино
    // ведь по правилам домино ни чего нам не мешает выложить их в ряд
    val isDominoCycle = EulerianCycle.hasEulerianCycle(dominSet)
    val isDominoLine = isDominoCycle || EulerianPath.hasEulerianPath(dominSet)
    val isRullesOfDominoes = isDominoLine || GraphGenerator.hasGeneratedGraph(dominSet)

    println(s"""Can you combine all the knuckles "Bazaar" on the rules of dominoes: $isRullesOfDominoes""")
    println(s"Is it possible to insert a bone in a row: $isDominoLine")
    println(s"Is it possible to close the knuckle to the ring: $isDominoCycle")
  }
}
