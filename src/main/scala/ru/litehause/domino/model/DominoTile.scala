package ru.litehause.domino.model

case class DominoTile(id: Int, left: Option[Int], right: Option[Int]) {
  def isTake = {
    left == right
  }
}

