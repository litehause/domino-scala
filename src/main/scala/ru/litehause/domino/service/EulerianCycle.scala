package ru.litehause.domino.service

import ru.litehause.domino.model.DominoTile

object EulerianCycle extends EulerianService {

  private def generateEilorCycle(left: Option[Int], right: Option[Int], tiles: List[DominoTile]): List[(Option[Int], Option[Int])] = {
    if (tiles.isEmpty) {
      (left, right) :: Nil
    } else {
      tiles.collect { case item if hasAddTile(left, right, item) =>
        val tilesExcludeCurrent = tiles.filterNot(_ == item)
        if (item.left == left) {
          generateEilorCycle(item.right, right, tilesExcludeCurrent)
        } else if (item.right == left) {
          generateEilorCycle(item.left, right, tilesExcludeCurrent)
        } else if (item.left == right) {
          generateEilorCycle(item.right, left, tilesExcludeCurrent)
        } else {
          generateEilorCycle(item.left, left, tilesExcludeCurrent)
        }
      }.flatten
    }
  }

  def hasEulerianCycle(tiles: List[DominoTile]): Boolean= {
    val countVertexOddDegree = tiles.foldLeft(List[Option[Int]]()){ case (result, item) =>
      item.left :: item.right :: result
    }.groupBy(item => item).count(_._2.size % 2 == 1)
    if (tiles.size == 0 || countVertexOddDegree != 0) {
      false
    } else {
      val headTile = tiles.head
      generateEilorCycle(headTile.left, headTile.right, tiles.drop(1))
        .exists(item => item._1 == item._2)
    }
  }
}
