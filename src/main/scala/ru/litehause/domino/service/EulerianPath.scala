package ru.litehause.domino.service

import ru.litehause.domino.model.DominoTile

/**
  *  Сдесь проверям можем ли мы выстроить костяшки в ряд
 */
object EulerianPath extends EulerianService {

  private def generateEilorPath(left: Option[Int], right: Option[Int], tiles: List[DominoTile]): Boolean  = {
    tiles.isEmpty || tiles.collect { case item if hasAddTile(left, right, item)  =>
       val tilesExcludeCurrent = tiles.filterNot(_ == item)
       (item.left == left && generateEilorPath(item.right, right, tilesExcludeCurrent)) ||
         (item.right == left && generateEilorPath(item.left, right, tilesExcludeCurrent)) ||
         (item.left == right && generateEilorPath(item.right, left, tilesExcludeCurrent)) ||
         (item.right == right && generateEilorPath(item.left, left, tilesExcludeCurrent))
     }.contains(true)
  }

  def hasEulerianPath(tiles: List[DominoTile]): Boolean= {
    val countVertexOddDegree = tiles.foldLeft(List[Option[Int]]()){ case (result, item) =>
      item.left :: item.right :: result
    }.groupBy(item => item).count(_._2.size % 2 == 1)
    if (tiles.size == 0 || countVertexOddDegree > 2) {
      false
    } else {
      val headTile = tiles.head
      generateEilorPath(headTile.left, headTile.right, tiles.drop(1))
    }
  }
}
