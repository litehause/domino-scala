package ru.litehause.domino.service

import ru.litehause.domino.model.DominoTile

trait EulerianService {

  protected def hasAddTile(left: Option[Int], right: Option[Int], tile: DominoTile) = {
    tile.left == left || tile.right == left || tile.left == right || tile.right == right
  }
}
