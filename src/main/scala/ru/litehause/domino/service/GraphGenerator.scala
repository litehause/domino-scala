package ru.litehause.domino.service

import ru.litehause.domino.model.DominoTile
/**
 * Построение графа из костяшек по правилам домино
 * одна костяшка с одной стороны может быть соединена только с 2 мя костяшками
 * при этом если это дубль например None - None, Some(1) - Some(1), ..... то к каждой стороне можно подсоединить 4 костяшки
 */
object GraphGenerator {

  private def hasAdd(vertex: List[Option[Int]], tile: DominoTile): Boolean = {
    vertex.contains(tile.left) || vertex.contains(tile.right)
  }

  private def genearateGraph(vertex: List[Option[Int]], tiles: List[DominoTile]): Boolean = {
    tiles.isEmpty || tiles.collect { case item if hasAdd(vertex, item) =>
      val tilesExcludeCurrent = tiles.filterNot(_ == item)
      (item.isTake && genearateGraph(item.left :: item.right :: vertex, tilesExcludeCurrent)) ||
        (vertex.contains(item.left) && genearateGraph(item.right :: vertex.diff(item.left :: Nil), tilesExcludeCurrent)) ||
        (vertex.contains(item.right) && genearateGraph(item.left :: vertex.diff(item.right :: Nil), tilesExcludeCurrent))
    }.contains(true)
  }


  def hasGeneratedGraph(tiles: List[DominoTile]): Boolean = {
    //сначало считаем количетв
    val takesVertex = tiles.filter(_.isTake)

    val countVertexOddDegree = tiles.filter(!_.isTake).foldLeft(List[Option[Int]]()){ case (result, item) =>
        item.left :: item.right :: result
    }.groupBy(vertex => vertex).map { case (vertex, vertexes) =>
      vertex -> vertexes.size
    }.count(_._2 % 2 == 1)

    if (tiles.isEmpty || countVertexOddDegree > 2 + takesVertex.size * 2) {
      false
    } else {
      val head = tiles.head
      val startVertex = if (head.isTake) {
        head.right :: head.right :: head.left :: head.left :: Nil
      } else {
        head.right :: head.left :: Nil
      }
      genearateGraph(startVertex, tiles.drop(1))
    }
  }
}
