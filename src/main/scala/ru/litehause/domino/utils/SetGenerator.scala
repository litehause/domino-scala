package ru.litehause.domino.utils

import ru.litehause.domino.model.DominoTile

object SetGenerator {

  private val numbers = None :: Some(1) :: Some(2) :: Some(3) :: Some(4) :: Some(5) :: Some(6) :: Nil

  private def generateDominoTile(number: Int, numbers: List[Option[Int]]) = {
    val i = (0 to numbers.length).foldLeft(number){ case (result, item) =>
      if (result >= item) {
        result - item
      } else {
        result
      }
    }
    DominoTile(number, numbers.last, numbers(i))
  }

  private def getSizeArray(number: Int, countInArray: Int): Int = {
    if (number <= countInArray) {
      countInArray
    } else getSizeArray(number - countInArray, countInArray + 1)
  }

  private def getCharsArray(number: Int) = {
    numbers.slice(0, getSizeArray(number, 1))
  }

  def generate(N: Int): List[DominoTile] = {
    if (N < 2 || N > 28) {
      throw new Exception("Count domino tile should be interval 2 < N < 28")
    }
    (0 until N)
      .map(i => generateDominoTile(i, getCharsArray(i + 1)))
      .toList
  }
}
